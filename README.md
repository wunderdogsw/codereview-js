# Code review assignment

🤔 HOW would you comment this in a PR review??

💩 DO try to find code smells and other bad practices !!

🔎 DON'T try to figure what the code does in detail !!

💾 DON'T worry about going through the entire file !!

🐌 DON'T think this is a speed contest !!

⏰ 10 MINUTES is the time you have for this !!

😎 REMEMBER to take it easy, you will do just fine !!