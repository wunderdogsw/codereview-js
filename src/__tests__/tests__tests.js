import fs from 'fs'
import { getdata } from '../server/server'

describe('tests', () => {
  it('get data should take between 1 - 5 seconds', (done) => {

    let finalValue = null
    let request = {}
    let request2 = {}

    //query with names and insert

    request.query = {insert: true, names: true}
    let response = {}
    response.json = (value) => finalValue = value
    response.send = (value) => finalValue = value


    let dataWithNames = false

    let circles = 0
    setInterval(() => {
      if (circles < 2 && finalValue) {
        throw new Error('test was too fast did data load right?')
      } else if (circles > 2000 && finalValue) {
        throw new Error('test was too slow')
      } else if (finalValue) {
        dataWithNames = finalValue
        // done()
      }
      circles = circles + 1
    }, 1)

    setTimeout(() => getdata(request, response), 50) // fix too fast

    // test data

    setInterval(() => {
      if (dataWithNames) {

        const results = []
        results.push(Object.keys(dataWithNames).length)
        for (const result of results) {
          console.log('RESPONSE', result)
          if (result !== 220) {
            throw new Error('error with data')
          }
        }
        done()

        // if (Object.keys(dataWithNames).length !== 220) {
        // }
      }
    }, 10)

  })
})
