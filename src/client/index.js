import React, { Component } from 'react'
import ReactDOM from 'react-dom'

let originalData = {}

class Update extends Component {
  render() {
    return <button onClick={() => this.props.onClick()}>Refresh</button>
  }
}

class Header extends Component {
  constructor(props) {
    super(props)
    this.state = {
      rows: Object.keys(props.rows).length
    }
  }

  render() {
    if (this.state.rows === 0) {
      return <h1>Loading <Update onClick={this.props.onClick}/></h1>
    } else {
      return <h1>Loaded {this.state.rows} <Update onClick={this.props.onClick}/></h1>
    }
  }
}

class App extends Component {

  constructor(props) {
    super(props)
    this.state = {
      load: false,
      data: {},
      names: false
    }
  }

  componentDidMount() {
    // (function(d, s, id){
    //   var js, fjs = d.getElementsByTagName(s)[0];
    //   if (d.getElementById(id)) {return;}
    //   js = d.createElement(s); js.id = id;
    //   js.src = "//connect.facebook.net/en_US/sdk.js";

    //   fjs.parentNode.insertBefore(js, fjs);
    // }(document, 'script', 'facebook-jssdk'));
  }

  componentWillMount() {
    this.setState({load: true})
    try {
      fetch('http://localhost:3131/api/data')
        .then(response => response.json())
        .then(data => this.state.data = data)
        .then(() => this.setState({load: false}))
    } catch (e) {
      // @todo
    }
  }

  render() {
    const a = this.state.data ? this.state.data.length === undefined ? 'o' : 'a' : null
    return <div>
      <Header rows={this.state.data} onClick={() => {
        this.setState({load: true})
        fetch('http://localhost:3131/api/data')
          .then(response => response.json())
          .then(data => this.state.data = data)
          .then(() => this.setState({load: false}))
      }}/>
      <input placeholder="filter by name" ref="filter" onKeyUp={() => {
        if (Object.keys(originalData).length === 0 && this.refs.filter.value) {
          originalData = this.state.data
          const fk = Object.keys(this.state.data).filter(key => {
            return originalData[key].name.toLowerCase().includes(this.refs.filter.value.toLowerCase())
          })

          this.setState({
            data: fk.reduce((obj, k) => {
              obj[k] = originalData[k]
              return obj
            }, {})
          })
        } else if (this.refs.filter.value) {
          const fk = Object.keys(this.state.data).filter(key => {
            return originalData[key] ? originalData[key].name.toLowerCase().includes(this.refs.filter.value.toLowerCase()) : false
          })
          this.setState({
            data: fk.reduce((obj, k) => {
              obj[k] = originalData[k]
              return obj
            }, {})
          })
        }
        else if (!this.refs.filter.value) {
          this.setState({data: originalData})
          originalData = {}
        }
      }}/>
      {a === 'o' ?
        Object.keys(this.state.data).map((key, index) => <h1 key={index}>{this.state.data[key].name}</h1>)
        : null}
      {a === 'a' ?
        this.state.data.map((key, index) => <h1 key={index}>{this.state.data.name}</h1>)
        : null}
    </div>
  }
}

ReactDOM.render(<App/>, document.getElementById('app'))
