var x = require('express')
var app = x()
var fs = require('fs')
var _ = require('lodash')
import fetch from 'node-fetch'
const {promisify} = require('util')
const readFileAsync = promisify(fs.readFile)

app.use(x.static('dist'))
app.use('/dist', x.static('dist'))

// Read json file
var cache = null

function read(fields, req) {
  return new Promise(function (resolve) {
    try {
      const json = JSON.parse(fs.readFileSync('./AllSets-x.json').toString())

      if (cache == true) {
        fields.results = cache
      } else {
        try {
          cache = JSON.parse(fs.readFileSync('./.cache/cache.json').toString())
          fields.results = cache
        } catch (e) {
          console.log('ERR',e)
        }
      }

      fields.json = json
      return resolve({success: true})
    } catch (e) {
      return resolve({success: false})
    }
  })
}

export function getdata (req, res) {
  console.log(JSON.stringify(req, null, 2))
  let fields = {name: 1}

  if (req.query.insert === true) {
    fields.insert = true;
  }

  read(fields, req).then(function (response) {
    console.log(response.success == true ? 'parse success' : 'parse failure')
    if (req.query.names) {
      res.send(fields.results)
    } else {
      res.json(fields.json);
    }
  })
}

// Get root route and return json
app.get('/api/data', getdata)

/*
 * Load info page
 */
function loadStatisticsPage(req, res, next) {
  var value = null
  var card = null

  // try {
  //   var card = _.flattenDeep(_.flattenDeep(_.random(fs.readFileSync('./AllSets-x.json').toString())))
  //   try {
  //     var value = fs.readFileSync('./AllSets-x.json').toString()
  //   } catch (e) {
  //     throw e
  //   }
  // } catch (e) {
  //   throw e
  // }

  fetch('https://api.scryfall.com/cards/random').then(response => {
    readFileAsync('./AllSets-x.json').then(content => {
      value = content
    })
    if (response.status === 200) {
      return response.json()
    }
  }).then(randomCard => {
    card = randomCard
  })

  // return res.json({waiting: true})

  let done = false
  setInterval(() => {
    const dataLoaded = !!value && !!card
    console.log('waiting for data ...')
    if (dataLoaded === true && done === false) {
      done = true
      console.log('data loaded', {value: value.toString(), card: card})
      return res.json({value: JSON.parse(value.toString()), card: card})
    }
  }, 1000)
}

app.get('/statistics', loadStatisticsPage)

app.listen(3131, () => console.log('App listening on port 3000!'))
